//package thegrace.canvashorizontallistview.component;
//
//import android.content.Context;
//import android.content.res.ColorStateList;
//import android.graphics.Canvas;
//import android.graphics.Paint;
//import android.graphics.PointF;
//import android.graphics.Rect;
//import android.graphics.RectF;
//import android.os.Build;
//import android.os.Handler;
//import android.os.Message;
//import android.support.v4.text.TextDirectionHeuristicCompat;
//import android.support.v4.view.ViewCompat;
//import android.text.BoringLayout;
//import android.text.TextPaint;
//import android.text.TextUtils;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.MotionEvent;
//import android.view.VelocityTracker;
//import android.view.View;
//
//import android.view.ViewConfiguration;
//import android.view.animation.DecelerateInterpolator;
//import android.widget.EdgeEffect;
//import android.widget.OverScroller;
//
//import android.view.animation.AnimationUtils;
//
//import com.thegrace.framework.Trace;
//
//import java.util.ArrayList;
//
//
//
//
///**
// * Created by limecm on 16. 5. 24..
// */
//public class CanvasHorizontalListView extends View  {
//
//    private final int       MSG_FLING_PROGRESS = 1;
//
//    private Rect    m_Rect    = new Rect();
//    private boolean m_bInit   = false;
//    private float   m_fScale  = 1.0f;
//
//    private ArrayList<AbstractCanvasItem> m_arrItems = new ArrayList<AbstractCanvasItem>();
//
//    private int                 m_nVisibleStartIndex     = 0;
//    private int                 m_nVisibleEndIndex       = 0;
//   // private int                 m_nVisible
//    private float               m_fVisibleStartX         = 0;
//    private float               m_fCurrScorllX           = 0;
//    private long                m_lPrevTime               = 0;
//
//
//   // private TouchHandler        m_TouchHandler           = null;
//
//    private AbstractCanvasItem  m_Header                 = null;
//    private AbstractCanvasItem  m_Footer                 = null;
//    private int                 m_Gravity                = Gravity.TOP;
//    private int                 m_nSelection              = -1;
//
//    /**
//     * The coefficient by which to adjust (divide) the max fling velocity.
//     */
//    private static final int SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT = 4;
//
//    /**
//     * The the duration for adjusting the selector wheel.
//     */
//    private static final int SELECTOR_ADJUSTMENT_DURATION_MILLIS = 800;
//
//    /**
//     * Determines speed during touch scrolling.
//     */
//    private VelocityTracker mVelocityTracker;
//
//    /**
//     * @see android.view.ViewConfiguration#getScaledMinimumFlingVelocity()
//     */
//    private int m_nMinimumFlingVelocity;
//
//    /**
//     * @see android.view.ViewConfiguration#getScaledMaximumFlingVelocity()
//     */
//    private int m_nMaximumFlingVelocity;
//
//    private int m_nOverscrollDistance;
//
//    private int m_nTouchSlop;
//
//    private int m_nPreviousScrollerX;
//
//
//    private float m_fLastDownEventX;
//
//    private OverScroller m_FlingScrollerX;
////    private OverScroller adjustScrollerX;
//
//
//
//    private boolean m_bScrollingX;
//    private int     m_nPressedItem = -1;
//
//
//
////    private int itemWidth;
//    private RectF itemClipBounds;
//    private RectF itemClipBoundsOffset;
//
//
//
//    private ColorStateList textColor;
//
//    private OnItemSelected onItemSelected;
//    private OnItemClicked onItemClicked;
//
//    private int selectedItem;
//
//    private EdgeEffect leftEdgeEffect;
//    private EdgeEffect rightEdgeEffect;
//
////    private Marquee marquee;
//    private int marqueeRepeatLimit = 3;
//
//    private float dividerSize = 0;
//
//    private int sideItems = 1;
//
//    private TextDirectionHeuristicCompat textDir;
//
////    private final PickerTouchHelper touchHelper;
//
//    private float m_nMaxWidth = 0;
//
//
//    public interface OnItemSelected {
//
//        public void onItemSelected(int index);
//
//    }
//
//    public interface OnItemClicked {
//
//        public void onItemClicked(int index);
//
//    }
//
//
////    private Handler             m_FlingHandler              = new Handler() {
////        public void handleMessage(Message msg) {
////            switch (msg.what) {
////                case MSG_FLING_PROGRESS: {
////                    Float velocity = (Float) msg.obj;
////                    float newVelocity = runMoveItem(velocity);
////                    if(Math.abs(newVelocity) < 1) {
////                         return;
////                    }
////                    Message msg2  = this.obtainMessage(MSG_FLING_PROGRESS);
////                    msg2.obj = new Float(newVelocity);
////                    this.sendMessageDelayed(msg2, 1000/30);
////                    break;
////                }
////            }
////        }
////    };
//
//    //GestureDetector;
//
//    public CanvasHorizontalListView(Context context) {
//        super(context);
//        init(context);
//    }
//
//    public CanvasHorizontalListView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        init(context);
//    }
//
//    public CanvasHorizontalListView(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init(context);
//    }
//
////    public CanvasHorizontalListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
////        super(context, attrs, defStyleAttr, defStyleRes);
////    }
//
//    private void init(Context context) {
//       // m_TouchHandler = new TouchHandler(getContext(), new MoveListener());
//
//        setWillNotDraw(false);
//
//        m_FlingScrollerX = new OverScroller(context);
////        adjustScrollerX = new OverScroller(context, new DecelerateInterpolator(2.5f));
//
//        // initialize constants
//        ViewConfiguration configuration = ViewConfiguration.get(context);
//        m_nTouchSlop = configuration.getScaledTouchSlop();
//        m_nMinimumFlingVelocity = configuration.getScaledMinimumFlingVelocity();
//        m_nMaximumFlingVelocity = configuration.getScaledMaximumFlingVelocity()
//                / SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT;
//        m_nOverscrollDistance = configuration.getScaledOverscrollDistance();
//
////        Trace.d(">> init overscrollDistance=" + overscrollDistance);
//        m_nPreviousScrollerX = Integer.MIN_VALUE;
//
////        setValues(values);
////        setSideItems(sideItems);
////
////        touchHelper = new PickerTouchHelper(this);
////        ViewCompat.setAccessibilityDelegate(this, touchHelper);
//
//        Trace.d("++ m_nTouchSlop = " + m_nTouchSlop);
//    }
//
//    @Override
//    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
//        super.onLayout(changed, left, top, right, bottom);
//
//        if(!m_bInit && changed) {
//            m_bInit = true;
//            m_Rect.set(left, top, right, bottom);
//            initView();
//        }
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//
//        int width = MeasureSpec.getSize(widthMeasureSpec);
//        int height = MeasureSpec.getSize(heightMeasureSpec);
//
//        setMeasuredDimension(width, height);
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//
////        calculateItemSize(w, h);
//    }
//
//    @Override
//    public void computeScroll() {
//
//        computeScrollX();
//    }
//
//    //  나중에 한번 확인 해봐야 함 !! 삭제하지 말것
////    @Override
////    public void setOverScrollMode(int overScrollMode) {
////        Trace.d(">> setOverScrollMode");
////        if(overScrollMode != OVER_SCROLL_NEVER) {
////            Context context = getContext();
////            leftEdgeEffect = new EdgeEffect(context);
////            rightEdgeEffect = new EdgeEffect(context);
////        } else {
////            leftEdgeEffect = null;
////            rightEdgeEffect = null;
////        }
////
////        super.setOverScrollMode(overScrollMode);
////    }
//
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        //super.onDraw(canvas);
//
//        Trace.d(">> onDraw");
//
//        canvas.save();
//        canvas.scale(m_fScale, m_fScale);
//        onDrawBackground(canvas);
//        onDrawItems(canvas);
//        canvas.restore();
//
////        drawEdgeEffect(canvas, leftEdgeEffect, 270);
////        drawEdgeEffect(canvas, rightEdgeEffect, 90);
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        //m_TouchHandler.onTouchEvent(event);
//        if(!isEnabled()) {
//            return false;
//        }
//
//        if (mVelocityTracker == null) {
//            mVelocityTracker = VelocityTracker.obtain();
//        }
//        mVelocityTracker.addMovement(event);
//
//
//        int action = event.getActionMasked();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//
//                // 플링이 끝난 후 반사 운동에 대한 처리
////                if(!adjustScrollerX.isFinished()) {
////                    adjustScrollerX.forceFinished(true);
////                } else
//
//
////                if(!flingScrollerX.isFinished()) {
////                    flingScrollerX.forceFinished(true);
////                } else {
////                    scrollingX = false;
////                }
//
//                m_fLastDownEventX = event.getX();
//
//                if(!m_bScrollingX) {
////                    pressedItem = getPositionFromTouch(event.getX());
//                }
//                invalidate();
//                break;
//
//            case MotionEvent.ACTION_MOVE:
//
//                float currentMoveX      = event.getX();
//                int deltaMoveX          = (int) (m_fLastDownEventX - currentMoveX);
//
//                if(m_bScrollingX || (Math.abs(deltaMoveX) > m_nTouchSlop) && m_arrItems != null && m_arrItems.size() > 0) {
//
//                    if(!m_bScrollingX) {
//                        deltaMoveX      = 0;
//                        m_nPressedItem  = -1;
//                        m_bScrollingX   = true;
//                        getParent().requestDisallowInterceptTouchEvent(true);
//                    }
//
////                    final int range = getScrollRange();
//
//                    if(runMoveItem(-deltaMoveX) == 0) {
//                        mVelocityTracker.clear();
//                    }
//
//
//                    Trace.d("+++ action move  curr scroll x= " + m_FlingScrollerX.getCurrX());
////                    if(overScrollBy(deltaMoveX, 0, getScrollX(), 0, range, 0,
////                            overscrollDistance, 0, true)) {
////                        mVelocityTracker.clear();
////                    }
//
////                    final float pulledToX = getScrollX() + deltaMoveX;
////                    if(pulledToX < 0) {
////                        leftEdgeEffect.onPull((float) deltaMoveX / getWidth());
////                        if(!rightEdgeEffect.isFinished()) {
////                            rightEdgeEffect.onRelease();
////                        }
////                    }
////                    else if(pulledToX > range) {
////                        rightEdgeEffect.onPull((float) deltaMoveX / getWidth());
////                        if(!leftEdgeEffect.isFinished()) {
////                            leftEdgeEffect.onRelease();
////                        }
////                    }
//
//                    m_fLastDownEventX = currentMoveX;
//                    invalidate();
//                }
//
//                break;
//
//            case MotionEvent.ACTION_UP:
//
//                VelocityTracker velocityTracker = mVelocityTracker;
//                velocityTracker.computeCurrentVelocity(1000, m_nMaximumFlingVelocity);
//                int initialVelocityX = (int) velocityTracker.getXVelocity();
//
//                if(m_bScrollingX && Math.abs(initialVelocityX) > m_nMinimumFlingVelocity) {
//                    flingX(initialVelocityX);
//                }
//                else if (m_arrItems != null) {
//                    float positionX = event.getX();
//                    if(!m_bScrollingX) {
//
//                        // 선택한 위치로 스크롤링
//
////                        int itemPos = getPositionOnScreen(positionX);
////                        int relativePos = itemPos - sideItems;
////
////                        if (relativePos == 0) {
////                            selectItem();
////                        } else {
////                            smoothScrollBy(relativePos);
////                        }
//
//                    } else if(m_bScrollingX) {
////                        finishScrolling();
//                    }
//                }
//
//                mVelocityTracker.recycle();
//                mVelocityTracker = null;
//
//                if(leftEdgeEffect != null) {
//                    leftEdgeEffect.onRelease();
//                    rightEdgeEffect.onRelease();
//                }
//
//            case MotionEvent.ACTION_CANCEL:
//                m_nPressedItem = -1;
//                invalidate();
//
//                if(leftEdgeEffect != null) {
//                    leftEdgeEffect.onRelease();
//                    rightEdgeEffect.onRelease();
//                }
//
//                break;
//        }
//
//        return true;
//
//    }
//
//
//
////    @Override
////    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
////        //return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
////
//////        Trace.d(">> overScrollBy ");
//////        return false;
////
////        moveItems(-deltaX);
////        return false;
////    }
//
//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//    }
//
//
//    @Override
//    protected void onDetachedFromWindow() {
//        super.onDetachedFromWindow();
//    }
//
//
//    private void selectItem() {
//        // post to the UI Thread to avoid potential interference with the OpenGL Thread
//        if (onItemClicked != null) {
//            post(new Runnable() {
//                @Override
//                public void run() {
////                    onItemClicked.onItemClicked(getSelectedItem());
//                }
//            });
//        }
//
////        adjustToNearestItemX();
//    }
//
////    private int getPositionOnScreen(float x) {
////        return (int) (x / (itemWidth + dividerSize));
////    }
//
//    protected void initView()
//    {
//    }
//
//    protected void onDrawBackground(Canvas canvas)
//    {
//    }
//
//    protected void onDrawItems(Canvas canvas)
//    {
//        int   visibleIndex  = m_nVisibleStartIndex;
//        Trace.d(">> onDrawItems visibleIndex=" + visibleIndex);
//        float x             = m_fVisibleStartX;
//        Trace.d(">> onDrawItems m_fVisibleStartX=" + m_fVisibleStartX);
//        int count           = m_arrItems.size();
//
//        if(count == 0)      return;
//
//        AbstractCanvasItem item = m_arrItems.get(0);
//        float h   = item.getHeight();
//        float y   = 0; //(m_Rect.height()/m_fScale - h)/2;
//        if(m_Gravity == Gravity.CENTER_VERTICAL) {
//            y = (m_Rect.height()/m_fScale - h)/2;
//        }
//        else if(m_Gravity == Gravity.BOTTOM) {
//            y = m_Rect.height()/m_fScale - h;
//        }
//
//        float screenW = m_Rect.width()/m_fScale - m_fVisibleStartX;
//
//        Trace.d( "++ onDrawItems visibleIndex=" + visibleIndex);
//        for(int i = visibleIndex; i < count; i++) {
//            item = m_arrItems.get(i);
//            canvas.save();
//            canvas.translate(x, y);
//            item.onDraw(canvas);
//            canvas.restore();
//            x += item.getWidth();
//
//            if(screenW <= 0) {
//                //Trace.d( "++ onDrawItems end index=" + i);
//                m_nVisibleEndIndex = i;
//                break;
//            }
//            screenW -= item.getWidth();
//        }
//    }
//
//    public void setScale(float scale) {
//        m_fScale = scale;
//        invalidate();
//    }
//
//    public void addItem(AbstractCanvasItem item) {
//        if(m_Footer != null) {
//            m_arrItems.add(m_arrItems.size()-1, item);
//        } else {
//            m_arrItems.add(item);
//        }
//
//        m_nMaxWidth += item.getWidth();
//
////        moveToEnd();
//        invalidate();
//    }
//
//    public void removeItem() {
//        if(m_Footer != null) {
//            m_arrItems.remove(m_arrItems.size()-2);
//        } else {
//            m_arrItems.remove(m_arrItems.size()-1);
//        }
//
//        //moveToEnd();
//        invalidate();
//    }
//
//    public void setSelectionItem(int index) {
//        m_nSelection = index;
//        invalidate();
//    }
//
//    public void moveToEnd() {
//
//        int startIndexFromEnd   = getStartItemIndexFromEnd();
//        float maxVisibleX       = getStartItemXFromEnd();
//
//        m_nVisibleStartIndex    = startIndexFromEnd;
//        m_fVisibleStartX        = maxVisibleX;
//
//        invalidate();
//    }
//
//    public boolean isVisibleItem(int index) {
//        if(index >= m_nVisibleStartIndex && index <= m_nVisibleEndIndex) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public void setHeader(AbstractCanvasItem header) {
//        if(m_Header != null) {
//            m_arrItems.remove(0);
//        }
//        if(header != null) {
//            m_arrItems.add(0, header);
//        }
//        m_Header = header;
//        invalidate();
//    }
//
//    public void setFooter(AbstractCanvasItem footer) {
//        if(m_Footer != null) {
//            m_arrItems.remove(m_Footer);
//        }
//        if(footer != null) {
//            m_arrItems.add(m_arrItems.size(), footer);
//        }
//        m_Footer = footer;
//        invalidate();
//    }
//
//    public void setGravity(int gravity) {
//        m_Gravity = gravity;
//    }
//
//    private int getStartItemIndexFromEnd() {
//        int count       = m_arrItems.size();
//        float screenW   = m_Rect.width()/m_fScale;
//        float w         = 0;
////        boolean isOverable = false;
//        for(int i = count-1; i >= 0; i--) {
//            AbstractCanvasItem item = m_arrItems.get(i);
//            w += item.getWidth();
//            if(w >= screenW) {
//                return i;
//            }
//        }
//        return 0;
//    }
//
//    private float getStartItemXFromEnd() {
//        int count       = m_arrItems.size();
//        float screenW   = m_Rect.width()/m_fScale;
//        float w         = 0;
//        boolean isOverable = false;
//        for(int i = count-1; i >= 0; i--) {
//            AbstractCanvasItem item = m_arrItems.get(i);
//            w += item.getWidth();
//            if(w >= screenW) {
//                return screenW-w;
//            }
//        }
//        return 0;
//    }
//
//
////    private class MoveListener extends TouchHandler.SimpleOnMoveGestureListener {
////
////        public boolean onMoveBegin(TouchHandler detector) {
////            m_FlingHandler.removeMessages(MSG_FLING_PROGRESS);
////            return true;
////        }
////
////        @Override
////        public boolean onMove(TouchHandler detector) {
////            PointF d = detector.getFocusDelta();
////            float distanceX = d.x/m_fScale;
////            moveItems(distanceX);
////            invalidate();
////            return true;
////        }
////
////        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
////        {
////            Trace.d( "++ onFling onFling velocityX =" + velocityX);
////            m_lPrevTime = System.currentTimeMillis();
////            Message msg  = m_FlingHandler.obtainMessage(MSG_FLING_PROGRESS);
////            msg.obj     = new Float(velocityX);
////            m_FlingHandler.sendMessage(msg);
////            return true;
////        }
////    }
//
//
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
//        {
//            Log.d("COMP", "++ onFling onFling velocityX =" + velocityX);
//            m_lPrevTime = AnimationUtils.currentAnimationTimeMillis();
//            Message msg  = m_FlingHandler.obtainMessage(MSG_FLING_PROGRESS);
//            msg.obj     = new Float(velocityX);
//            m_FlingHandler.sendMessage(msg);
//            return true;
//        }
//
//
//    private static final float DISTANCE_CONVERT_FACTOR = 2.5f;	// 속도로부터 스크롤 거리를 계산할 때 곱하는 상수, 값이 클 수록 스크롤 양이 커짐
//    private static final float FRICTION_FACTOR = 0.8f;	// 화면 업데이트 될 때 마다 속도를 늦춰주는 상수
//
//    private float runMoveItem(float velocityX) {
//        long curTime        = AnimationUtils.currentAnimationTimeMillis();
//        long timeGap        = curTime - m_lPrevTime;
//        //float distanceX     = timeGap * velocityX/1000;
//        float distanceX     = velocityX/1000 * timeGap * DISTANCE_CONVERT_FACTOR;
//        Log.d("COMP", "++ onFling moveItems distanceX =" + distanceX);
//
//        if(Math.abs(distanceX) < 1) {
//            Trace.d( "-- onFling onFling distanceX =" + distanceX);
//            return 0;
//        }
//        moveItems(distanceX);
//        invalidate();
//
//
//        //float newVelocityX = velocityX - velocityX/10;
//        float newVelocityX = velocityX * FRICTION_FACTOR;	// 속도를 줄임.
//        Log.d("COMP", "++ onFling onFling newVelocityX =" + newVelocityX);
//        if(Math.abs(newVelocityX) < 1) {
//            Log.d("COMP", "-- onFling onFling newVelocityX =" + newVelocityX);
//            return 0;
//        }
//
//
////        m_lPrevTime     = curTime;
//        return 1;
//    }
//
//    private void moveItems(float distanceX) {
//
//        Log.d("COMP", "++ m_fVisibleStartX distanceX=" + distanceX);
//        Log.d("COMP", "++ m_fVisibleStartX=" + m_fVisibleStartX);
//
//
//        float   prevVisibleX        = m_fVisibleStartX + distanceX;
//        int     prevVisibleIndex    = m_nVisibleStartIndex;
//        m_fCurrScorllX              += -distanceX;
//
//
////        Log.w("COMP", "++ m_fVisibleStartX prevVisibleX=" + prevVisibleX);
//        if(distanceX < 0) {
//            while (true) {
//                AbstractCanvasItem item = m_arrItems.get(prevVisibleIndex);
//                int startIndexFromEnd   = getStartItemIndexFromEnd();
//                Trace.d( "++ m_fVisibleStartX startIndexFromEnd=" + startIndexFromEnd);
//                if(startIndexFromEnd <= prevVisibleIndex) {
//                    prevVisibleIndex = startIndexFromEnd;
//                    float maxVisibleX = getStartItemXFromEnd();
//
//                    if(maxVisibleX > prevVisibleX) {
//                        prevVisibleX = maxVisibleX;
//                    }
//                    break;
//                }
//
//                if(prevVisibleX + item.getWidth() >= 0) {
//                    Trace.d( "++ m_fVisibleStartX +++ caculate=" + (prevVisibleX + item.getWidth()));
//                    break;
//                }
//                prevVisibleX += item.getWidth();
//                prevVisibleIndex++;
////                Log.e("COMP", "++ m_fVisibleStartX +++ prevVisibleX=" + prevVisibleX);
//            }
//
//            m_fVisibleStartX        = prevVisibleX;
//            m_nVisibleStartIndex = prevVisibleIndex;
//
////            Log.e("COMP", "++ m_fVisibleStartX=" + m_fVisibleStartX);
//        }
//        else if(distanceX > 0) {
//            while (true) {
//                if(prevVisibleIndex < 0) {
//                    prevVisibleX = 0;
//                    prevVisibleIndex = 0;
//                    break;
//                }
//
//                if(prevVisibleIndex == 0 && prevVisibleX > 0) {
//                    prevVisibleX = 0;
//                    prevVisibleIndex = 0;
//                    break;
//                }
//
//                if(prevVisibleX > 0) {
//                    prevVisibleIndex--;
//                    AbstractCanvasItem item = m_arrItems.get(prevVisibleIndex);
//                    prevVisibleX -= item.getWidth();
//                    if(prevVisibleX< 0) {
//                        break;
//                    }
//                    prevVisibleX -= item.getWidth();
//                    prevVisibleIndex--;
//                }
//                else {
//                    break;
//                }
//            }
//
//            m_fVisibleStartX        = prevVisibleX;
//            m_nVisibleStartIndex    = prevVisibleIndex;
//
////            Log.e("COMP", "++ end m_fVisibleStartX2=" + m_fVisibleStartX);
//
//        }
//    }
//
//    private void drawEdgeEffect(Canvas canvas, EdgeEffect edgeEffect, int degrees) {
//
//        if (canvas == null || edgeEffect == null || (degrees != 90 && degrees != 270)) {
//            return;
//        }
//
//        if(!edgeEffect.isFinished()) {
//            final int restoreCount = canvas.getSaveCount();
//            final int width = getWidth();
//            final int height = getHeight();
//
//            canvas.rotate(degrees);
//
//            if (degrees == 270) {
//                canvas.translate(-height, Math.max(0, getScrollX()));
//            } else { // 90
////                canvas.translate(0, -(Math.max(getScrollRange(), getScaleX()) + width));
//            }
//
//            edgeEffect.setSize(height, width);
//            if(edgeEffect.draw(canvas)) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    postInvalidateOnAnimation();
//                } else {
//                    postInvalidate();
//                }
//            }
//
//            canvas.restoreToCount(restoreCount);
//        }
//
//    }
//
//    public int getItemCount() {
//        return m_arrItems.size();
//    }
//
//    public AbstractCanvasItem getItem(int index) {
//        return m_arrItems.get(index);
//    }
//
//// 삭제하지 말것
////    @Override
////    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
////        super.scrollTo(scrollX, scrollY);
////
////        if(!flingScrollerX.isFinished() && clampedX) {
//////            flingScrollerX.springBack(scrollX, scrollY, 0, getScrollRange(), 0, 0);
////        }
////    }
//
////    @Override
////    protected void drawableStateChanged() {
////        super.drawableStateChanged(); //TODO
////    }
//
////    private int getPositionFromTouch(float x) {
////        return getPositionFromCoordinates((int) (getScrollX() - (itemWidth + dividerSize) * (sideItems + .5f) + x));
////    }
//
//    /**
//     * Calculates item from x coordinate position.
//     * @param x Scroll position to calculate.
//     * @return Selected item from scrolling position in {param x}
//     */
////    private int getPositionFromCoordinates(int x) {
////        return Math.round(x / (itemWidth + dividerSize));
////    }
//
//
//    private void computeScrollX() {
//        Trace.d(">> computeScrollX count" );
//
//        OverScroller scroller = m_FlingScrollerX;
//        if(scroller.isFinished()) {
//            Trace.d("++ scroller.isFinished() = true");
////            scroller = adjustScrollerX;
////            if(scroller.isFinished()) {
////                Trace.d("++ scroller.isFinished() = true");
////                return;
////            }
//            return;
//        }
//
//        if(scroller.computeScrollOffset()) {
//            int currentScrollerX = scroller.getCurrX();
//            Trace.d("++ scroller.computeScrollOffset() currentScrollerX= " + currentScrollerX);
//
//
//            if(m_nPreviousScrollerX == Integer.MIN_VALUE) {
//                m_nPreviousScrollerX = scroller.getStartX();
//                Trace.d("++ m_nPreviousScrollerX = " + m_nPreviousScrollerX);
//            }
//
////            int range = getScrollRange();
////            if(previousScrollerX >= 0 && currentScrollerX < 0) {
////                leftEdgeEffect.onAbsorb((int) scroller.getCurrVelocity());
////            } else if(previousScrollerX <= range && currentScrollerX > range) {
////                rightEdgeEffect.onAbsorb((int) scroller.getCurrVelocity());
////            }
//
//            runMoveItem(-(currentScrollerX - m_nPreviousScrollerX));
////
////            overScrollBy(currentScrollerX - previousScrollerX, 0, previousScrollerX, getScrollY(),
////                    getScrollRange(), 0, overscrollDistance, 0, false);
//
//            m_nPreviousScrollerX = currentScrollerX;
////            Trace.d("++ scroller.computeScrollOffset() = previousScrollerX=>" + previousScrollerX);
////            if(scroller.isFinished()) {
////                onScrollerFinishedX(scroller);
////            }
//
////            scrollTo(0,0);
//            postInvalidate();
////            postInvalidateOnAnimation(); // TODO
//        }
//        else {
//            Trace.d("++ scroller.computeScrollOffset() = false");
//        }
//    }
//
//    private void flingX(int velocityX) {
//
//        Trace.d(">> flingX  velocityX= " + velocityX);
//        Trace.d(">> flingX  velocityX m_fVisibleStartX= " + m_fVisibleStartX);
//        Trace.d(">> flingX  velocityX m_nMaxWidth= " + m_nMaxWidth);
//
//        m_nPreviousScrollerX = Integer.MIN_VALUE;
//        //m_FlingScrollerX.fling((int)m_FlingScrollerX.getCurrX(), 0, -velocityX, 0, 0, (int)m_nMaxWidth, 0, 0, getWidth() / 2, 0);
//
//        int currX = (int)m_fCurrScorllX;
//        m_FlingScrollerX.fling((int)currX, 0, -velocityX, 0, 0, (int)m_nMaxWidth, 0, 0, getWidth() / 2, 0);
//
//        invalidate();
//    }
//
////    private void adjustToNearestItemX() {
////
////        int x = getScrollX();
////        int item = Math.round(x / (itemWidth + dividerSize * 1f));
////
////        if(item < 0) {
////            item = 0;
////        } else if(item > m_arrItems.size()) {
////            item = m_arrItems.size();
////        }
////
////        selectedItem = item;
////
////        int itemX = (itemWidth + (int) dividerSize) * item;
////
////        int deltaX = itemX - x;
////
////        previousScrollerX = Integer.MIN_VALUE;
////        adjustScrollerX.startScroll(x, 0, deltaX, 0, SELECTOR_ADJUSTMENT_DURATION_MILLIS);
////        invalidate();
////    }
//
////    private void calculateItemSize(int w, int h) {
////
////        int items = sideItems * 2 + 1;
////        int totalPadding = ((int) dividerSize * (items - 1));
////        itemWidth = (w - totalPadding) / items;
////
////        itemClipBounds = new RectF(0, 0, itemWidth, h);
////        itemClipBoundsOffset = new RectF(itemClipBounds);
////
//////        scrollToItem(selectedItem);
////
//////        remakeLayout();
//////        startMarqueeIfNeeded();
////
////    }
//
//
//
//
////    private void onScrollerFinishedX(OverScroller scroller) {
////        if(scroller == flingScrollerX) {
////            finishScrolling();
////        }
////    }
//
////    private void finishScrolling() {
////
//////        adjustToNearestItemX();
////        scrollingX = false;
//////        startMarqueeIfNeeded();
////        // post to the UI Thread to avoid potential interference with the OpenGL Thread
////        if (onItemSelected != null) {
////            post(new Runnable() {
////                @Override
////                public void run() {
//////                    onItemSelected.onItemSelected(getPositionFromCoordinates(getScrollX()));
////                }
////            });
////        }
////    }
//
////    private void smoothScrollBy(int i) {
////        int deltaMoveX = (itemWidth + (int) dividerSize) * i;
////        deltaMoveX = getRelativeInBound(deltaMoveX);
////
////        previousScrollerX = Integer.MIN_VALUE;
////        flingScrollerX.startScroll(getScrollX(), 0, deltaMoveX, 0);
//////        stopMarqueeIfNeeded();
////        invalidate();
////    }
//
//    /**
//     * Scrolls to specified item.
//     * @param index Index of an item to scroll to
//     */
////    private void scrollToItem(int index) {
////        scrollTo((itemWidth + (int) dividerSize) * index, 0);
////        // invalidate() not needed because scrollTo() already invalidates the view
////    }
//
//    /**
//     * Calculates relative horizontal scroll position to be within our scroll bounds.
////     * {com_wefika.horizontalpicker.HorizontalPicker#getInBoundsX(int)}
//     * @param x Relative scroll position to calculate
//     * @return Current scroll position + {param x} if is within our scroll bounds, otherwise it
//     * will return min/max scroll position.
//     */
////    private int getRelativeInBound(int x) {
////        int scrollX = getScrollX();
////        return getInBoundsX(scrollX + x) - scrollX;
////    }
//
//
//    /**
//     * Calculates x scroll position that is still in range of view scroller
//     * @param x Scroll position to calculate.
//     * @return {param x} if is within bounds of over scroller, otherwise it will return min/max
//     * value of scoll position.
//     */
////    private int getInBoundsX(int x) {
////
////        if(x < 0) {
////            x = 0;
////        } else if(x > ((itemWidth + (int) dividerSize) * (m_arrItems.size() - 1))) {
////            x = ((itemWidth + (int) dividerSize) * (m_arrItems.size() - 1));
////        }
////        return x;
////    }
////
////    private int getScrollRange() {
////        int scrollRange = 0;
////        if(m_arrItems != null && m_arrItems.size() != 0) {
////            scrollRange = Math.max(0, ((itemWidth + (int) dividerSize) * (m_arrItems.size() - 1)));
////        }
////        return scrollRange;
////    }
//
//
//}
