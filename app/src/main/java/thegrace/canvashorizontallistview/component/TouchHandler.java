//package thegrace.canvashorizontallistview.component;
//
//import android.content.Context;
//import android.graphics.PointF;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.VelocityTracker;
//import android.view.ViewConfiguration;
//import android.view.animation.DecelerateInterpolator;
//import android.widget.OverScroller;
//
//
//public class TouchHandler  {
//
//	/**
//	 * Listener which must be implemented which is used by MoveGestureDetector
//	 * to perform callbacks to any implementing class which is registered to a
//	 * MoveGestureDetector via the constructor.
//	 *
//	 * @see SimpleOnMoveGestureListener
//	 */
//	public interface OnMoveGestureListener {
//		public boolean onMove(TouchHandler detector);
//		public boolean onMoveBegin(TouchHandler detector);
//		public void onMoveEnd(TouchHandler detector);
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);
//	}
//
//	/**
//	 * Helper class which may be extended and where the methods may be
//	 * implemented. This way it is not necessary to implement all methods
//	 * of OnMoveGestureListener.
//	 */
//	public static class SimpleOnMoveGestureListener implements OnMoveGestureListener {
//	    public boolean onMove(TouchHandler detector) {
//	        return false;
//	    }
//
//	    public boolean onMoveBegin(TouchHandler detector) {
//	        return true;
//	    }
//
//	    public void onMoveEnd(TouchHandler detector) {
//	    	// Do nothing, overridden implementation may be used
//	    }
//
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
//        {
//            return false;
//        }
//
//	}
//
//    private static final PointF FOCUS_DELTA_ZERO = new PointF();
//
//    private final OnMoveGestureListener mListener;
//
//    private PointF mCurrFocusInternal;
//    private PointF mPrevFocusInternal;
//    private PointF mFocusExternal = new PointF();
//    private PointF mFocusDeltaExternal = new PointF();
//
//    private Context m_Context;
//
//    protected boolean mGestureInProgress;
//
//    protected MotionEvent mPrevEvent;
//    protected MotionEvent mCurrEvent;
//
//    protected float mCurrPressure;
//    protected float mPrevPressure;
//    protected long mTimeDelta;
//
//<<<<<<< HEAD
//    /**
//     * The coefficient by which to adjust (divide) the max fling velocity.
//     */
//    private static final int SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT = 4;
//
//=======
//    private static final int UNIT_PIXELS_PER_MILLI = 1; 	// Velocity trackter 의 시간 단위 - 1밀리초
//>>>>>>> 37eafa2549d7e97084a95db9ab4b17285eebb048
//
//    /**
//     * This value is the threshold ratio between the previous combined pressure
//     * and the current combined pressure. When pressure decreases rapidly
//     * between events the position values can often be imprecise, as it usually
//     * indicates that the user is in the process of lifting a pointer off of the
//     * device. This value was tuned experimentally.
//     */
//    protected static final float PRESSURE_THRESHOLD = 0.67f;
//
//
//    /**
//     * Determines speed during touch scrolling
//     */
//    private VelocityTracker mVelocityTracker;
//
//    /**
//     * @see android.view.ViewConfiguration#getScaledMinimumFlingVelocity()
//     */
//    private int mMinimumFlingVelocity;
//    /**
//     * @see android.view.ViewConfiguration#getScaledMaximumFlingVelocity()
//     */
//    private int mMaximumFlingVelocity;
//
//
//
//    private MotionEvent mCurrentDownEvent;
//
//    private final int overscrollDistance;
//
//    private int touchSlop;
//
//    private float lastDownEventX;
//
//    private OverScroller flingScrollerX;
//    private OverScroller adjustScrollerX;
//
//    private int previousScrollerX;
//
//    private boolean scrollingX;
//    private int pressedItem = -1;
//
//
//    public TouchHandler(Context context, OnMoveGestureListener listener) {
//        m_Context = context;
//        mListener = listener;
//
//        flingScrollerX = new OverScroller(context);
//        adjustScrollerX = new OverScroller(context, new DecelerateInterpolator(2.5f));
//
//        final ViewConfiguration configuration = ViewConfiguration.get(context);
//
//        mMinimumFlingVelocity = configuration.getScaledMinimumFlingVelocity();
//        mMaximumFlingVelocity = configuration.getScaledMaximumFlingVelocity()/SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT;
//        overscrollDistance = configuration.getScaledOverscrollDistance();
//        touchSlop = configuration.getScaledTouchSlop();
//
//        previousScrollerX = Integer.MIN_VALUE;
//    }
//
//    public boolean onTouchEvent(MotionEvent event){
//
//        if (mVelocityTracker == null) {
//            mVelocityTracker = VelocityTracker.obtain();
//        }
//        mVelocityTracker.addMovement(event);
//
//        final int actionCode = event.getActionMasked();
//        switch (actionCode) {
//            case MotionEvent.ACTION_DOWN:
//                if (mCurrentDownEvent != null) {
//                    mCurrentDownEvent.recycle();
//                }
//                mCurrentDownEvent = MotionEvent.obtain(event);
//                break;
//
//            case MotionEvent.ACTION_MOVE:
//
//                break;
//
//            case MotionEvent.ACTION_UP:
//                // A fling must travel the minimum tap distance
//                final VelocityTracker velocityTracker = mVelocityTracker;
//                final int pointerId = event.getPointerId(0);
//                velocityTracker.computeCurrentVelocity(1000, mMaximumFlingVelocity);
//                final float velocityY = velocityTracker.getYVelocity(pointerId);
//                final float velocityX = velocityTracker.getXVelocity(pointerId);
//
//                if ((Math.abs(velocityY) > mMinimumFlingVelocity)
//                        || (Math.abs(velocityX) > mMinimumFlingVelocity)){
//
//                    mListener.onFling(mCurrentDownEvent, event, velocityX, velocityY);
//                }
//                break;
//            case MotionEvent.ACTION_CANCEL:
//
//                break;
//
//        }
//
//        if (!mGestureInProgress) {
//            handleStartProgressEvent(actionCode, event);
//        } else {
//            handleInProgressEvent(actionCode, event);
//        }
//        return true;
//    }
//
//
//    protected void handleStartProgressEvent(int actionCode, MotionEvent event){
//        switch (actionCode) {
//            case MotionEvent.ACTION_DOWN:
//                resetState(); // In case we missed an UP/CANCEL event
//
//                mPrevEvent = MotionEvent.obtain(event);
//                mTimeDelta = 0;
//
//                updateStateByEvent(event);
//                break;
//
//            case MotionEvent.ACTION_MOVE:
//                mGestureInProgress = mListener.onMoveBegin(this);
//                break;
//        }
//    }
//
//
//    protected void handleInProgressEvent(int actionCode, MotionEvent event){
//        switch (actionCode) {
//        	case MotionEvent.ACTION_UP:
//            case MotionEvent.ACTION_CANCEL:
//                mListener.onMoveEnd(this);
//                resetState();
//                break;
//
//            case MotionEvent.ACTION_MOVE:
//                updateStateByEvent(event);
//
//				// Only accept the event if our relative pressure is within
//				// a certain limit. This can help filter shaky data as a
//				// finger is lifted.
//                if (mCurrPressure / mPrevPressure > PRESSURE_THRESHOLD) {
//                    Log.d("COMP", "+++++ move_action ");
//                    final boolean updatePrevious = mListener.onMove(this);
//                    if (updatePrevious) {
//                        mPrevEvent.recycle();
//                        mPrevEvent = MotionEvent.obtain(event);
//                    }
//                }
//                break;
//        }
//	}
//
//    protected void updateStateByEvent(MotionEvent curr) {
//        final MotionEvent prev = mPrevEvent;
//
//        // Reset mCurrEvent
//        if (mCurrEvent != null) {
//            mCurrEvent.recycle();
//            mCurrEvent = null;
//        }
//        mCurrEvent = MotionEvent.obtain(curr);
//
//
//        // Delta time
//        mTimeDelta = curr.getEventTime() - prev.getEventTime();
//
//        // Pressure
//        mCurrPressure = curr.getPressure(curr.getActionIndex());
//        mPrevPressure = prev.getPressure(prev.getActionIndex());
//
//
//
//        // Focus intenal
//        mCurrFocusInternal = determineFocalPoint(curr);
//        mPrevFocusInternal = determineFocalPoint(prev);
//
//        // Focus external
//        // - Prevent skipping of focus delta when a finger is added or removed
//        boolean mSkipNextMoveEvent = prev.getPointerCount() != curr.getPointerCount();
//        mFocusDeltaExternal = mSkipNextMoveEvent ? FOCUS_DELTA_ZERO : new PointF(mCurrFocusInternal.x - mPrevFocusInternal.x,  mCurrFocusInternal.y - mPrevFocusInternal.y);
//
//        // - Don't directly use mFocusInternal (or skipping will occur). Add
//        // 	 unskipped delta values to mFocusExternal instead.
//        mFocusExternal.x += mFocusDeltaExternal.x;
//        mFocusExternal.y += mFocusDeltaExternal.y;
//    }
//
//	/**
//	 * Determine (multi)finger focal point (a.k.a. center point between all
//	 * fingers)
//	 *
//	 * @param MotionEvent e
//	 * @return PointF focal point
//	 */
//    private PointF determineFocalPoint(MotionEvent e){
//    	// Number of fingers on screen
//        final int pCount = e.getPointerCount();
//        float x = 0f;
//        float y = 0f;
//
//        for(int i = 0; i < pCount; i++){
//        	x += e.getX(i);
//        	y += e.getY(i);
//        }
//
//        return new PointF(x/pCount, y/pCount);
//    }
//
//    public float getFocusX() {
//        return mFocusExternal.x;
//    }
//
//    public float getFocusY() {
//        return mFocusExternal.y;
//    }
//
//    public PointF getFocusDelta() {
//		return mFocusDeltaExternal;
//    }
//
//
//
//    protected void resetState() {
//        if (mPrevEvent != null) {
//            mPrevEvent.recycle();
//            mPrevEvent = null;
//        }
//        if (mCurrEvent != null) {
//            mCurrEvent.recycle();
//            mCurrEvent = null;
//        }
//        mGestureInProgress = false;
//    }
//
//
//    /**
//     * Returns {@code true} if a gesture is currently in progress.
//     * @return {@code true} if a gesture is currently in progress, {@code false} otherwise.
//     */
//    public boolean isInProgress() {
//        return mGestureInProgress;
//    }
//
//    /**
//     * Return the time difference in milliseconds between the previous accepted
//     * GestureDetector event and the current GestureDetector event.
//     *
//     * @return Time difference since the last move event in milliseconds.
//     */
//    public long getTimeDelta() {
//        return mTimeDelta;
//    }
//
//    /**
//     * Return the event time of the current GestureDetector event being
//     * processed.
//     *
//     * @return Current GestureDetector event time in milliseconds.
//     */
//    public long getEventTime() {
//        return mCurrEvent.getEventTime();
//    }
//}
