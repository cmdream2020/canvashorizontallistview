package com.sample;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sample.canvashorizontallistview.R;
import com.thegrace.framework.component.CanvasHorizontalListView.AbstractCanvasItem;
import com.thegrace.framework.component.CanvasHorizontalListView.CanvasHorizontalListView;


public class MainActivity extends AppCompatActivity {

    private CanvasHorizontalListView m_ListView;

    private class Symbol extends AbstractCanvasItem {

        private Paint m_Paint;
        private Paint m_Paint2;
        private int   m_nIndex;

        private float m_nWidth  = 0;
        private float m_nHeight = 0;

        public Symbol(int index) {
            m_nIndex = index;

            m_Paint = new Paint();
            m_Paint.setColor(Color.RED);
            m_Paint.setStyle(Paint.Style.STROKE);

            m_Paint2 = new Paint();
            m_Paint2.setColor(Color.BLUE);
            m_Paint2.setStyle(Paint.Style.STROKE);

            if(index % 2 == 0) {
                m_nWidth = 60;
            } else {
                m_nWidth = 120;
            }

            m_nHeight = 100;

        }

        @Override
        public float getWidth() {
            return m_nWidth;
        }

        @Override
        public float getHeight() {
            return m_nHeight;
        }

        @Override
        public boolean isDeletable() {
            return false;
        }

        @Override
        public boolean isSelectable() {
            return false;
        }

        @Override
        public void onDraw(Canvas canvas, boolean bSelected) {
            canvas.drawRect(5, 5, getWidth()-5, getHeight()-5, m_Paint);
            String num = String.valueOf(m_nIndex);
            Rect rect  = new Rect();
            m_Paint.getTextBounds(num, 0, num.length(), rect);

//            Log.d("COMP", "rect w="+ rect.width() + ", h=" + rect.height());
//            Log.d("COMP", "pow x="+ ((getWidth()-rect.width())/2) + ", y=" + ((getHeight()-rect.height())/2));

//            canvas.drawText(num, (getWidth()-rect.width())/2, (getHeight()-rect.height())/2, m_Paint);

            canvas.drawText(num, (getWidth()-rect.width())/2, (getHeight()-rect.height())/2 + rect.height(), m_Paint);
//            canvas.drawLine((getWidth()-rect.width())/2, 0, (getWidth()-rect.width())/2, getHeight(), m_Paint);
//            canvas.drawLine(0, (getHeight()-rect.height())/2, getWidth(), (getHeight()-rect.height())/2, m_Paint);

            canvas.drawLine(getWidth()/2, 0, getWidth()/2, getHeight(), m_Paint2);
            canvas.drawLine(0, getHeight()/2, getWidth(), getHeight()/2, m_Paint2);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_ListView = (CanvasHorizontalListView)findViewById(R.id.canvas_list_view);

        for(int i = 0; i < 10; i++) {
            m_ListView.addItem(new Symbol(i+1));
        }

        m_ListView.setHeader(new Symbol(9999));
        m_ListView.setFooter(new Symbol(9997));

        m_ListView.setScale(3.0f);
    }
}
