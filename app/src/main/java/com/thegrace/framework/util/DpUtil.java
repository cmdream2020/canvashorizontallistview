package com.thegrace.framework.util;


import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;


public class DpUtil {
	

	
	public static int		dpToPixel(Context context, float dp)
	{
        if(dp == 0)
            return 0;

		Resources r = context.getResources();
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
		return (int) px;
	}

    public static int pixelToDp(Context context, int pixel) {

        DisplayMetrics metrics =  context.getResources().getDisplayMetrics();

        float dp = pixel / (metrics.densityDpi / 160f);

        return (int) dp;

    }


}
