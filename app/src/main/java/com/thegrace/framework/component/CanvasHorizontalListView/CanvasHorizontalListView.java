package com.thegrace.framework.component.CanvasHorizontalListView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Scroller;

import com.thegrace.framework.Trace;

import java.util.ArrayList;


/**
 * Created by limecm on 16. 5. 24..
 */
public class CanvasHorizontalListView extends View implements GestureDetector.OnGestureListener {

    private final int       MSG_FLING_PROGRESS = 1;

    private Rect    m_Rect    = new Rect();
    private boolean m_bInit   = false;
    private float   m_fScale  = 1.0f;

    private ArrayList<AbstractCanvasItem> m_arrItems = new ArrayList<AbstractCanvasItem>();

    private int                 m_nVisibleStartIndex     = 0;
    private int                 m_nVisibleEndIndex       = 0;
    private float               m_fVisibleStartX         = 0;
    private int                 m_fNewStartX             = 0;

    private AbstractCanvasItem  m_Header                 = null;
    private AbstractCanvasItem  m_Footer                 = null;
    private int                 m_Gravity                = Gravity.TOP;
    private int                 m_nSelection              = -1;

    private GestureDetector     m_Detector;
    protected Scroller          mScroller;


    private Handler             m_FlingHandler              = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case MSG_FLING_PROGRESS: {

//                    Trace.d("++ m_fNewStartX=" + m_fNewStartX);
                    if(mScroller.computeScrollOffset()) {
//                        Trace.d("++ mScroller.computeScrollOffset() true curx=" + mScroller.getCurrX());
                        int scrollX = mScroller.getCurrX();
                        float dX = (m_fNewStartX-scrollX)/m_fScale;

                        //float dx = m_fVisibleStartX - m_fNewStartX;
                        //Trace.d("++ dx-" + dX);

                        if(dX != 0) {
                            moveItems(dX);
                            invalidate();
                        }
                        m_fNewStartX = scrollX;
                    }


                    if(!mScroller.isFinished()) {
                        Trace.d("++ notfinished =" + m_fNewStartX);
                        m_FlingHandler.sendEmptyMessage(MSG_FLING_PROGRESS);
                    }

                    break;
                }
            }
        }
    };

    //GestureDetector;

    public CanvasHorizontalListView(Context context) {
        super(context);
        init(context);
    }

    public CanvasHorizontalListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CanvasHorizontalListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

//    public CanvasHorizontalListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    private void init(Context context) {
        mScroller  = new Scroller(context);
        m_Detector = new GestureDetector(context, this);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if(!m_bInit && changed) {
            m_bInit = true;
            Trace.d(">> onLayout left=" + left + ", right=" + right);
            m_Rect.set(left, top, right, bottom);
            initView();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.scale(m_fScale, m_fScale);
        onDrawBackground(canvas);
        onDrawItems(canvas);
        canvas.restore();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        m_Detector.onTouchEvent(event);
        return true;
    }




    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }




    protected void initView()
    {
    }

    protected void onDrawBackground(Canvas canvas)
    {
    }

    protected void onDrawItems(Canvas canvas)
    {
        //Trace.d(">> onDrawItems x = " + m_fVisibleStartX);
        int   visibleIndex  = m_nVisibleStartIndex;
        float x             = m_fVisibleStartX;
        int count           = m_arrItems.size();

        if(count == 0)      return;

        AbstractCanvasItem item = m_arrItems.get(0);
        float h   = item.getHeight();
        float y   = 0; //(m_Rect.height()/m_fScale - h)/2;
        if(m_Gravity == Gravity.CENTER_VERTICAL) {
            y = (m_Rect.height()/m_fScale - h)/2;
        }
        else if(m_Gravity == Gravity.BOTTOM) {
            y = m_Rect.height()/m_fScale - h;
        }

        float screenW = m_Rect.width()/m_fScale - m_fVisibleStartX;

        //Log.d("COMP", "++ onDrawItems visibleIndex=" + visibleIndex);
        for(int i = visibleIndex; i < count; i++) {
            item = m_arrItems.get(i);
            canvas.save();
            canvas.translate(x, y);
            item.onDraw(canvas, m_nSelection == i);
            canvas.restore();
            x += item.getWidth();

            if(screenW <= 0) {
                //Log.d("COMP", "++ onDrawItems end index=" + i);
                m_nVisibleEndIndex = i;
                break;
            }
            screenW -= item.getWidth();
        }
    }

    public void setScale(float scale) {
        m_fScale = scale;
        invalidate();
    }

    public void addItem(AbstractCanvasItem item) {
        if(m_Footer != null) {
            m_arrItems.add(m_arrItems.size()-1, item);
            setSelectionItem(m_arrItems.size()-2);
        } else {
            m_arrItems.add(item);
            setSelectionItem(m_arrItems.size()-1);
        }
        postInvalidate();
    }

    public boolean isLastSelected()
    {
        if(m_Footer != null) {
            if(m_nSelection >= m_arrItems.size()-2) {
                return true;
            }
        }
        else {
            if(m_nSelection >= m_arrItems.size()-1) {
                return true;
            }
        }
        return false;
    }

    public void addItem(int index, AbstractCanvasItem item) {
        m_arrItems.add(index, item);
        setSelectionItem(index);
        //moveToEnd();
        postInvalidate();
    }

    public void removeItem(int index) {
        if (m_arrItems.get(index).isDeletable()) {
            m_arrItems.remove(index);
            if (index == m_nSelection) {
                m_nSelection--;
                while (m_nSelection >= 0) {
                    AbstractCanvasItem item = m_arrItems.get(m_nSelection);
                    if (!item.isSelectable() && item.isDeletable()) {
                        m_arrItems.remove(m_nSelection);
                        m_nSelection--;
                        continue;
                    } else {
                        break;
                    }
                }
            }
        }
        invalidate();
    }

    public void clearSelection()
    {
        m_nSelection = -1;
        invalidate();
    }

    public void setSelectionItem(int index) {
        if(m_arrItems.get(index).isSelectable()) {
            m_nSelection = index;
            moveToSelection();

            invalidate();
        }
    }

    public int getSelectionItem() {
        return m_nSelection;
    }

    public void moveToSelection()
    {
        if(isVisibleItem(m_nSelection)) {
            return;
        }
        int startIndexFromEnd   = getStartItemIndexFromSelection();
        float maxVisibleX       = getStartItemXFromSelection();

        m_nVisibleStartIndex    = startIndexFromEnd;
        m_fVisibleStartX        = maxVisibleX;
    }

    public void moveToStart() {
        m_nVisibleStartIndex    = 0;
        m_fVisibleStartX         = 0;
        invalidate();
    }


    public void moveToEnd() {
        if(m_nVisibleEndIndex < m_nSelection) {
            return;
        }

        int startIndexFromEnd   = getStartItemIndexFromEnd();
        float maxVisibleX       = getStartItemXFromEnd();

        m_nVisibleStartIndex    = startIndexFromEnd;
        m_fVisibleStartX        = maxVisibleX;

        Trace.d("++ moveToEnd = " + m_nVisibleStartIndex);
        Trace.d("++ moveToEnd m_fVisibleStartX = " + m_fVisibleStartX);

        postInvalidate();
    }

    public boolean isVisibleItem(int index) {
        if(index >= m_nVisibleStartIndex && index <= m_nVisibleEndIndex) {
            return true;
        } else {
            return false;
        }
    }

    public void setHeader(AbstractCanvasItem header) {
        if(m_Header != null) {
            m_arrItems.remove(0);
        }
        if(header != null) {
            m_arrItems.add(0, header);
        }
        m_Header = header;
        invalidate();
    }

    public void setFooter(AbstractCanvasItem footer) {
        if(m_Footer != null) {
            m_arrItems.remove(m_Footer);
        }
        if(footer != null) {
            m_arrItems.add(m_arrItems.size(), footer);
        }
        m_Footer = footer;
        invalidate();
    }

    public void setGravity(int gravity) {
        m_Gravity = gravity;
    }


    /**
     /* 아이템의 끝으로 부터 시작하는 아이템의 인덱스를 구함
     /**/
    private int getStartItemIndexFromSelection() {
        int count       = m_nSelection;
        float screenW   = m_Rect.width()/m_fScale;
        float w         = 0;

        for(int i = count-1; i >= 0; i--) {
            AbstractCanvasItem item = m_arrItems.get(i);
            w += item.getWidth();
            if(w >= screenW/2) {
                return i;
            }
        }
        return 0;
    }

    /**
     /* 아이템의 끝으로 부터 시작하는 아이템의 X 좌표를 구함
     /**/
    private float getStartItemXFromSelection() {
        int count       = m_nSelection;
        float screenW   = m_Rect.width()/m_fScale;
        float w         = 0;
        boolean isOverable = false;
        for(int i = count-1; i >= 0; i--) {
            AbstractCanvasItem item = m_arrItems.get(i);
            w += item.getWidth();
            if(w >= screenW/2) {
                return screenW/2-w;
            }
        }
        return 0;
    }

    /**
     /* 아이템의 끝으로 부터 시작하는 아이템의 인덱스를 구함
     /**/
    private int getStartItemIndexFromEnd() {
        int count       = m_arrItems.size();
        float screenW   = m_Rect.width()/m_fScale;
        float w         = 0;
        Trace.d("++ getStartItemIndexFromEnd screenW=" + screenW + ", m_Rect.width()=" + m_Rect.width());
//        boolean isOverable = false;
        for(int i = count-1; i >= 0; i--) {
            AbstractCanvasItem item = m_arrItems.get(i);
            w += item.getWidth();
            if(w >= screenW) {
                return i;
            }
        }
        return 0;
    }

    /**
    /* 아이템의 끝으로 부터 시작하는 아이템의 X 좌표를 구함
    /**/
    private float getStartItemXFromEnd() {
        int count       = m_arrItems.size();
        float screenW   = m_Rect.width()/m_fScale;
        float w         = 0;
        boolean isOverable = false;
        for(int i = count-1; i >= 0; i--) {
            AbstractCanvasItem item = m_arrItems.get(i);
            w += item.getWidth();
            if(w >= screenW) {
                return screenW-w;
            }
        }
        return 0;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        Trace.d(">> onDown...");
        //m_FlingHandler.removeMessages(MSG_FLING_PROGRESS);

        m_FlingHandler.removeMessages(MSG_FLING_PROGRESS);
        mScroller.forceFinished(true);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();
        Trace.d(">> onSingleTap= x=" + x + ", y=" + y);
        int sel = findTouchItem(x, y);
        if(sel >= 0) {
            if(m_arrItems.get(sel).isSelectable() == false ) {
                sel = -1;
            }
        }
        if(sel >= 0)  {
            setSelectionItem(sel);
        }
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        //PointF d = detector.getFocusDelta();
        Trace.d(">> onScroll...");
        float dX = -distanceX/m_fScale;
        moveItems(dX);
        invalidate();
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }



    // 참고 해보자 
    //https://code.tutsplus.com/tutorials/android-sdk-introduction-to-gestures--mobile-2239

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Trace.d(">> onFling...vx=" +velocityX + ", e1=" + e1.getX() + ", e2=" + e2.getX());
        m_fNewStartX = Integer.MAX_VALUE/2;
        mScroller.fling(m_fNewStartX, 0, (int)-velocityX, 0, 0, Integer.MAX_VALUE, 0, 0);
        m_FlingHandler.sendEmptyMessage(MSG_FLING_PROGRESS);
        return true;
    }


    private boolean moveItems(float distanceX) {
        //Log.d("COMP", "++ m_fVisibleStartX distanceX=" + distanceX);
        //Log.d("COMP", "++ m_fVisibleStartX=" + m_fVisibleStartX);

        float   prevVisibleX        = m_fVisibleStartX + distanceX;
        int     prevVisibleIndex    = m_nVisibleStartIndex;

        //Log.w("COMP", "++ m_fVisibleStartX prevVisibleX=" + prevVisibleX);
        if(distanceX < 0) {
            while (true) {
                AbstractCanvasItem item = m_arrItems.get(prevVisibleIndex);
                int startIndexFromEnd   = getStartItemIndexFromEnd();
                //Log.d("COMP", "++ m_fVisibleStartX startIndexFromEnd=" + startIndexFromEnd);
                if(startIndexFromEnd <= prevVisibleIndex) {
                    prevVisibleIndex = startIndexFromEnd;
                    float maxVisibleX = getStartItemXFromEnd();

                    if(maxVisibleX > prevVisibleX) {
                        prevVisibleX = maxVisibleX;
                    }
                    break;
                }

                if(prevVisibleX + item.getWidth() >= 0) {
                    //Log.d("COMP", "++ m_fVisibleStartX +++ caculate=" + (prevVisibleX + item.getWidth()));
                    break;
                }
                prevVisibleX += item.getWidth();
                prevVisibleIndex++;
                //Log.e("COMP", "++ m_fVisibleStartX +++ prevVisibleX=" + prevVisibleX);
            }

            m_fVisibleStartX        = prevVisibleX;
            m_nVisibleStartIndex = prevVisibleIndex;

            //Log.e("COMP", "++ m_fVisibleStartX=" + m_fVisibleStartX);
        }
        else if(distanceX > 0) {
            while (true) {
                if(prevVisibleIndex < 0) {
                    prevVisibleX = 0;
                    prevVisibleIndex = 0;
                    break;
                }

                if(prevVisibleIndex == 0 && prevVisibleX > 0) {
                    prevVisibleX = 0;
                    prevVisibleIndex = 0;
                    break;
                }

                if(prevVisibleX > 0) {
                    prevVisibleIndex--;
                    AbstractCanvasItem item = m_arrItems.get(prevVisibleIndex);
                    prevVisibleX -= item.getWidth();
                    if(prevVisibleX< 0) {
                        break;
                    }
                    prevVisibleX -= item.getWidth();
                    prevVisibleIndex--;
                }
                else {
                    break;
                }
            }

            m_fVisibleStartX        = prevVisibleX;
            m_nVisibleStartIndex = prevVisibleIndex;

            //Log.e("COMP", "++ end m_fVisibleStartX2=" + m_fVisibleStartX);



        }
        return true;
    }

    public int getItemCount() {
        return m_arrItems.size();
    }

    public AbstractCanvasItem getItem(int index) {
        return m_arrItems.get(index);
    }

    private int findTouchItem(float touchX, float touchY) {
        int   visibleIndex  = m_nVisibleStartIndex;
        float x             = m_fVisibleStartX;
        int count           = m_arrItems.size();

        if(count == 0)      return -1;

        AbstractCanvasItem item = m_arrItems.get(0);
        float h   = item.getHeight();
        float y   = 0; //(m_Rect.height()/m_fScale - h)/2;
        if(m_Gravity == Gravity.CENTER_VERTICAL) {
            y = (m_Rect.height()/m_fScale - h)/2;
        }
        else if(m_Gravity == Gravity.BOTTOM) {
            y = m_Rect.height()/m_fScale - h;
        }

        float screenW = m_Rect.width()/m_fScale - m_fVisibleStartX;

        //Log.d("COMP", "++ onDrawItems visibleIndex=" + visibleIndex);
        for(int i = visibleIndex; i < count; i++) {
            item = m_arrItems.get(i);
            float x2 = x + item.getWidth();

            if(x < touchX && x2 > touchX && y < touchY && (y+h) > touchY) {
                return i;
            }
            x = x2;
            if(screenW <= 0) {
                break;
            }
            screenW -= item.getWidth();
        }
        return -1;
    }


    // 특정 거리만큼 이동함
    private synchronized void scrollTo(int moveX) {
        m_fNewStartX = Integer.MAX_VALUE/2;
        mScroller.startScroll(m_fNewStartX, 0, moveX, 0);
        m_FlingHandler.sendEmptyMessage(MSG_FLING_PROGRESS);
    }
}
