package com.thegrace.framework.component.CanvasHorizontalListView;

import android.graphics.Canvas;

/**
 * Created by limecm on 16. 5. 24..
 */
public abstract class AbstractCanvasItem {

    public abstract float    getWidth();
    public abstract float    getHeight();
    public abstract void     onDraw(Canvas canvas, boolean bSelected);
    public abstract boolean  isDeletable();
    public abstract boolean  isSelectable();
}
